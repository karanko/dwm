#! /bin/bash


brightness () {
    
let max_brightness=$(cat /sys/class/backlight/intel_backlight/max_brightness)
let current_brightness=$(cat /sys/class/backlight/intel_backlight/brightness)
let step_size=$max_brightness/30
let next_step=$current_brightness+$step_size 
let prev_step=$current_brightness-$step_size 

if [[ $next_step -gt $max_brightness ]]; then next_step=$max_brightness 
fi
if [[ $prev_step -lt 0 ]]; then prev_step=1
fi

if [[ $1 == "up" ]]; then 
echo up
echo $(echo $next_step | sudo tee /sys/class/backlight/intel_backlight/brightness)
elif [[ $1 == "down" ]]; then 
echo down
echo $(echo $prev_step | sudo tee /sys/class/backlight/intel_backlight/brightness)
fi

}
vol () {
if [[ $1 == "up" ]]; then 
#echo up
amixer -q sset Master 2%+ 
amixer -q sset Master $(amixer  sget Master | grep "Front Left: Playback" | cut -d[ -f2 | cut -d] -f1) unmute
elif [[ $1 == "down" ]]; then 
#echo down
amixer -q sset Master 2%- 
amixer -q sset Master $(amixer  sget Master | grep "Front Left: Playback" | cut -d[ -f2 | cut -d] -f1) unmute
elif [[ $1 == "toggle" ]]; then 
#echo toggle
amixer -q sset Master toggle
fi

}


compton_toggle () {
 
 $(pkill compton || compton -i 0.90 -o .70 -e 0.5  -t -5  -m 1 -c -C -r 6 -b --focus-exclude "x = 0 && y = 0 && override_redirect = true || class_g ?= 'thunderbird' || class_g ?= 'vlc'" )

}
background_solid (){
	xsetroot -solid $1
}

background () {

wallpaper=$(find ~/Pictures/Wallpapers -maxdepth 1   -type f | shuf | tail -1 )
echo $wallpaper
#$(convert "$wallpaper" -posterize 6 -colorspace gray .wallpaper.jpg)
$(cat "$wallpaper" > ~/.wallpaper.jpg)
$(feh  --no-fehbg --bg-fill ~/.wallpaper.jpg)
}

add_bigscreen () {
    xrandr --output DP-1-2 --mode 3440x1440 --above eDP-1
	feh   --no-fehbg --bg-fill ~/.wallpaper.jpg 
	
}
search (){
    surf https://duckduckgo.com/?kae=d&q
}

if [[ $1 == "brightness" ]]; then brightness $2
elif [[ $1 == "vol" ]]; then vol $2
elif [[ $1 == "compton" ]]; then compton_toggle
elif [[ $1 == "background" ]]; then background
elif [[ $1 == "background-solid" ]]; then background_solid $2
elif [[ $1 == "add_bigscreen" ]]; then add_bigscreen
elif [[ $1 == "search" ]]; then search
fi
