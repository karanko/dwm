FROM debian:stable

RUN apt update && \
	apt install libx11-dev libx11-xcb-dev libxcb-res0-dev libxinerama-dev libxft-dev \
        	libncurses5-dev libelf-dev libssl-dev libxrandr-dev libimlib2-dev libxtst-dev \
        	libpam0g-dev build-essential cmake git bc flex bison


