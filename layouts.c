void monocle(Monitor *m)
{
	Client *c;
	if (m->showbar){
		for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
			resize(c, m->wx + 10 , m->wy + 10 , m->ww - (c->bw*2) - 20, m->wh - (c->bw*2) - 20, False);
	}
	else {
		for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
			resize(c, m->wx , m->wy , m->ww - (c->bw*2), m->wh - (c->bw*2), False);
	}
}

void apple_layout(Monitor *m)
{
	unsigned int n = 0;
	Client *c;
	unsigned int cwidth = ((m->showbar ? (m->mfact * 100.0) : 100.0) / 100.0) * m->ww;
	unsigned int x = m->wx + ((m->ww - cwidth) / 3.5);
	unsigned int gap_px = m->showbar ? gappx : 0;
	for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
		if (ISVISIBLE(c))
			n++;
	if (n == 1 && !m->showbar){
		monocle(m);
		return;
	}
	if (n > 1) /* override layout symbol */
		snprintf(m->ltsymbol, sizeof m->ltsymbol, "s%d ", n);

	for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
		resize(c, x, m->wy + (gap_px*3), cwidth - c->bw, m->wh - ((c->bw + (gap_px*4)) * 2), 0);
}
void speed_dating(Monitor *m)
{
	unsigned int n = 0;
	Client *c;
	unsigned int cwidth = ((m->showbar ? (m->mfact * 100.0) : 100.0) / 100.0) * m->ww;
	unsigned int x = m->wx + ((m->ww - cwidth) / 2);
	unsigned int gap_px = m->showbar ? gappx : 0;
	for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
		if (ISVISIBLE(c))
			n++;
	if (n == 1 && !m->showbar){
		monocle(m);
		return;
	}
	if (n > 1) /* override layout symbol */
		snprintf(m->ltsymbol, sizeof m->ltsymbol, "s%d ", n);

	for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
		resize(c, x, m->wy + gap_px, cwidth - c->bw, m->wh - ((c->bw + gap_px) * 2), 0);
}
void fixed_dual(Monitor *m) {

	unsigned int i = 0;
	Client *c;
	unsigned int gap_px = m->showbar ? gappx : 0;


  snprintf(m->ltsymbol, sizeof m->ltsymbol, "d");

	for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next))
  {
    if(!ISVISIBLE(c))
      continue;

    unsigned int cwidth =  (m->ww / 2) - ((gap_px * 1.5) + (c->bw*2));
    unsigned int cheight =m->wh - ((c->bw + gap_px) * (2));


    if(i== 0)
		  resize(c, gap_px, m->wy + gap_px, cwidth, cheight, 0);
		else if(i==1)
		  resize(c, cwidth + (gap_px*2), m->wy + gap_px, cwidth, cheight, 0);
		else
			return;

    i++;
  }
}

void fixed_quad(Monitor *m) {

	unsigned int i = 0;
	Client *c;

  snprintf(m->ltsymbol, sizeof m->ltsymbol, "q");

	for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next))
  {
    if(!ISVISIBLE(c))
      continue;

    unsigned int cwidth =  (m->ww / 2) - (c->bw * 2);
    unsigned int cheight = (m->wh / 2) - (c->bw * 2);


    if(i== 0)
		  resize(c, 0, m->wy , cwidth, cheight, 0);
		else if(i==1)
		  resize(c, cwidth , m->wy , cwidth, cheight, 0);
		else if(i==2)
		  resize(c, 0 , m->wy + cheight, cwidth, cheight, 0);
		else if(i==3)
		  resize(c, cwidth , m->wy + cheight, cwidth, cheight, 0);
		else
			return;

    i++;
  }
}

void zoom(const Arg *arg)
{
	Client *c = selmon->sel;

	if (!selmon->lt[selmon->sellt]->arrange || (selmon->sel && selmon->sel->isfloating))
		return;
	if (c == nexttiled(selmon->clients))
		if (!c || !(c = nexttiled(c->next)))
			return;
	pop(c);
}
static void
bstack(Monitor *m)
{
	int w, h, mh, mx, tx, ty, tw;
	unsigned int i, n;
	Client *c;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++)
		;
	if (n == 0)
		return;
	if (n == 1)
	{
		 monocle(m);
		 return;
	}
	if (n > m->nmaster)
	{
		mh = m->nmaster ? m->mfact * m->wh : 0;
		tw = m->ww / (n - m->nmaster);
		ty = m->wy + mh;
	}
	else
	{
		mh = m->wh;
		tw = m->ww;
		ty = m->wy;
	}
	for (i = mx = 0, tx = m->wx, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
	{
		if (i < m->nmaster)
		{
			w = (m->ww - mx) / (MIN(n, m->nmaster) - i);
			resize(c, m->wx + mx, m->wy, w - (2 * c->bw), mh - (2 * c->bw), 0);
			mx += WIDTH(c);
		}
		else
		{
			h = m->wh - mh;
			resize(c, tx, ty, tw - (2 * c->bw), h - (2 * c->bw), 0);
			if (tw != m->ww)
				tx += WIDTH(c);
		}
	}
}
void tile(Monitor *m)
{
	unsigned int i, n, h, mw, my, ty;
	Client *c;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++)
		if (n == 0)
			return;

	if (n > m->nmaster)
		mw = m->nmaster ? m->ww * m->mfact : 0;
	else
		mw = m->ww - m->gap->gappx;
	for (i = 0, my = ty = m->gap->gappx, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
		if (i < m->nmaster)
		{
			h = (m->wh - my) / (MIN(n, m->nmaster) - i) - m->gap->gappx;
			if (n == 1)
			{
				resize(c, m->wx + m->gap->gappx, m->wy + my, mw - (2 * c->bw) - m->gap->gappx, h - (2 * c->bw), 0);
				if (my + HEIGHT(c) + m->gap->gappx < m->wh)
					my += HEIGHT(c) + m->gap->gappx;
			}
			else
			{
				resize(c, m->wx - c->bw, m->wy + my, mw - c->bw, h - c->bw, False);
			}
		}
		else
		{
			h = (m->wh - ty) / (n - i) - m->gap->gappx;
			resize(c, m->wx + mw + m->gap->gappx, m->wy + ty, m->ww - mw - (2 * c->bw) - 2 * m->gap->gappx, h - (2 * c->bw), 0);
			if (ty + HEIGHT(c) + m->gap->gappx < m->wh)
				ty += HEIGHT(c) + m->gap->gappx;
		}
}
void centeredmaster(Monitor *m)
{
	unsigned int i, n, h, mw, mx, my, oty, ety, tw;
	Client *c;

	/* count number of clients in the selected monitor */
	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++)
		;
	if (n == 0)
		return;
	if (n == 1)
	{
		 monocle(m);
		 return;
	}
	/* initialize areas */
	mw = m->ww;
	mx = 0;
	my = 0;
	tw = mw;

	if (n > m->nmaster)
	{
		/* go mfact box in the center if more than nmaster clients */
		mw = m->nmaster ? m->ww * m->mfact : 0;
		tw = m->ww - mw;

		if (n - m->nmaster > 1)
		{
			/* only one client */
			mx = (m->ww - mw) / 2;
			tw = (m->ww - mw) / 2;
		}
	}

	oty = 0;
	ety = 0;
	for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
		if (i < m->nmaster)
		{
			/* nmaster clients are stacked vertically, in the center
			 * of the screen */
			h = (m->wh - my) / (MIN(n, m->nmaster) - i);
			resize(c, m->wx + mx, m->wy + my, mw - (2 * c->bw),
				   h - (2 * c->bw), 0);
			my += HEIGHT(c);
		}
		else
		{
			/* stack clients are stacked vertically */
			if ((i - m->nmaster) % 2)
			{
				h = (m->wh - ety) / ((1 + n - i) / 2);
				resize(c, m->wx, m->wy + ety, tw - (2 * c->bw),
					   h - (2 * c->bw), 0);
				ety += HEIGHT(c);
			}
			else
			{
				h = (m->wh - oty) / ((1 + n - i) / 2);
				resize(c, m->wx + mx + mw, m->wy + oty,
					   tw - (2 * c->bw), h - (2 * c->bw), 0);
				oty += HEIGHT(c);
			}
		}
}

void fibonacci(Monitor *m) {
  unsigned int i, s = 0, nx, ny, nw, nh, b;
  unsigned int n = 0;
  unsigned int gap_px = m->showbar ? gappx : 0;
  Client *c;
  for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
    if (ISVISIBLE(c))
      n++;

	if (n == 1 && !m->showbar){
		monocle(m);
		return;
	}

  if (n == 0)
    return;
  if (n == 1)
    b = 0;
  nx = m->wx;
  ny = 0;
  nw = m->ww;
  nh = m->wh - gap_px;

  for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next)) {
    if (!ISVISIBLE(c))
      continue;
    if ((i % 2 && nh / 2 > 2 * b) || (!(i % 2) && nw / 2 > 2 * b)) {
      if (i < n - 1) {
        if (i % 2)
          nh /= 2;
        else
          nw /= 2;
        if ((i % 4) == 2 && !s)
          nx += nw;
        else if ((i % 4) == 3 && !s)
          ny += nh;
      }
      if ((i % 4) == 0) {
        if (s)
          ny += nh;
        else
          ny -= nh;
      } else if ((i % 4) == 1)
        nx += nw - gap_px;
      else if ((i % 4) == 2)
        ny += nh;
      else if ((i % 4) == 3) {
        if (s)
          nx += nw;
        else
          nx -= nw;
      }
      if (i == 0) {
        //	if(n != 1)
          nw = (m->ww * m->mfact);
        ny = m->wy;
      } else if (i == 1)
        nw = (m->ww - nw);
      i++;
    }
    if (i == 1)
      nw -= gap_px;
    resize(c, nx + gap_px, ny + gap_px, nw - ((2 * (b)) + gap_px),
           nh - ((2 * (b)) + gap_px), 0);
    if (i == 1)
      nw += gap_px;
  }
}
void
tatami(Monitor *m) {
	unsigned int i, n, nx, ny, nw, nh,
				 mats, tc,
				 tnx, tny, tnw, tnh;
	Client *c;

	for(n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), ++n);
	if(n == 0)
		return;

	nx = m->wx;
	ny = 0;
	nw = m->ww;
	nh = m->wh;

	c = nexttiled(m->clients);

	if(n != 1)  nw = m->ww * m->mfact;
				ny = m->wy;

	resize(c, nx, ny, nw - 2 * c->bw, nh - 2 * c->bw, False);

	c = nexttiled(c->next);

	nx += nw;
	nw = m->ww - nw;

	if(n>1)
	{

	tc = n-1;
	mats = tc/5;

	nh/=(mats + (tc % 5 > 0));

	for(i = 0; c && (i < (tc % 5)); c = nexttiled(c->next))
	{
		tnw=nw;
		tnx=nx;
		tnh=nh;
		tny=ny;
		switch(tc - (mats*5))
				{
					case 1://fill
						break;
					case 2://up and down
						if((i % 5) == 0) //up
						tnh/=2;
						else if((i % 5) == 1) //down
						{
							tnh/=2;
							tny += nh/2;
						}
						break;
					case 3://bottom, up-left and up-right
						if((i % 5) == 0) //up-left
						{
						tnw = nw/2;
						tnh = (2*nh)/3;
						}
						else if((i % 5) == 1)//up-right
						{
							tnx += nw/2;
							tnw = nw/2;
							tnh = (2*nh)/3;
						}
						else if((i % 5) == 2)//bottom
						{
							tnh = nh/3;
							tny += (2*nh)/3;
						}
						break;
					case 4://bottom, left, right and top
						if((i % 5) == 0) //top
						{
							tnh = (nh)/4;
						}
						else if((i % 5) == 1)//left
						{
							tnw = nw/2;
							tny += nh/4;
							tnh = (nh)/2;
						}
						else if((i % 5) == 2)//right
						{
							tnx += nw/2;
							tnw = nw/2;
							tny += nh/4;
							tnh = (nh)/2;
						}
						else if((i % 5) == 3)//bottom
						{
							tny += (3*nh)/4;
							tnh = (nh)/4;
						}
						break;
				}
		++i;
		resize(c, tnx, tny, tnw - 2 * c->bw, tnh - 2 * c->bw, False);
	}

	++mats;

	for(i = 0; c && (mats>0); c = nexttiled(c->next)) {

			if((i%5)==0)
			{
			--mats;
			if(((tc % 5) > 0)||(i>=5))
			ny+=nh;
			}

			tnw=nw;
			tnx=nx;
			tnh=nh;
			tny=ny;


			switch(i % 5)
			{
				case 0: //top-left-vert
					tnw = (nw)/3;
					tnh = (nh*2)/3;
					break;
				case 1: //top-right-hor
					tnx += (nw)/3;
					tnw = (nw*2)/3;
					tnh = (nh)/3;
					break;
				case 2: //center
					tnx += (nw)/3;
					tnw = (nw)/3;
					tny += (nh)/3;
					tnh = (nh)/3;
					break;
				case 3: //bottom-right-vert
					tnx += (nw*2)/3;
					tnw = (nw)/3;
					tny += (nh)/3;
					tnh = (nh*2)/3;
					break;
				case 4: //(oldest) bottom-left-hor
					tnw = (2*nw)/3;
					tny += (2*nh)/3;
					tnh = (nh)/3;
					break;
				default:
					break;
			}

			++i;
			//i%=5;
		resize(c, tnx, tny, tnw - 2 * c->bw, tnh - 2 * c->bw, False);
		}
	}
}


void auto_layout(Monitor *m) {
  unsigned int n = 0;
  Client *c;
  for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
      if (ISVISIBLE(c))
        n++;

  if (n == 0) {
    snprintf(m->ltsymbol, sizeof m->ltsymbol, "a");
    return;
  }
	else if (n == 1) {
     //apple_layout(m);
  	monocle(m);
 		return;
  }
  else if (n == 2) {
    fixed_dual(m);
    return;
  }
  else if (n == 4) {
    fixed_quad(m);
    return;
  }

  tatami(m);
}
