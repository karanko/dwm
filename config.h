/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx = 1; /* border pixel of windows */
static const unsigned int gappx = 20;	/* gap pixel of windows */
static const Gap default_gap = {.isgap = 0, .realgap = 0, .gappx = 0};
static const unsigned int snap = 10;		/* snap pixel */
static const char panel[][20]       = { "xfce4-panel", "Xfce4-panel" }; /* name & cls of panel win */
static const int showbar = 1;				/* 0 means no bar */
static const int topbar = 1;				/* 0 means bottom bar */
static const char *fonts[] = {
//	"Sans:style=Normal:size=10",
//	"Consolas:style=Regular:size=12",
//	"Charcoal:style=Regular:size=12:antialias=false",
//	"SFMono Nerd Font,SF Mono:style=Light:size=12",
//	"Terminus:style=Medium:size=12",
//	"Droid Sans:size=12",
//	"NotoMono Nerd Font Mono:size=12",
	"Noto Sans:style=Regular:size=11",
//	"JetBrains Mono,JetBrains Mono:size=12",
//	"Segoe UI:size=11",
	"monospace:style=bold:size=10:antialias=false"
	};

static const char col_gray1[] = "#8c8c8c";
static const char col_gray2[] = "#dedede";
static const char col_gray3[] = "#efefef";
static const char col_gray4[] = "#323232";
static const char col_black[] = "#000000";
static const char col_white[] = "#ffffff";
//static const char col_cyan[] = "#8048e4";
static const char col_cyan[] = "#ff98ab";

static const unsigned int baralpha = 64;
static const unsigned int borderalpha = 64;

static const char *colors[][3] = {
	/*               fg         bg         border   */
	[SchemeSel] = {   col_black,col_white ,col_gray4},
	[SchemeNorm] = {col_white, col_black, col_black},
	[SchemeTabActive] = {col_black, col_white, col_white},
	[SchemeTabInactive] = {col_black, col_white, col_white}};
static const unsigned int alphas[][3] = {
	/*               fg      bg        border     */
	[SchemeNorm] = {OPAQUE, baralpha, borderalpha},
	[SchemeSel] = {OPAQUE, baralpha, borderalpha},
	[SchemeTabActive] = {OPAQUE, baralpha, borderalpha},
	[SchemeTabInactive] = {OPAQUE, baralpha, borderalpha},
};
// static const char *const autostart[] = {
// 	"DISPLAY=:0 st", NULL,
// 	NULL /* terminate */
// };
static const char *brightness_up[] = {"dwm-utils.sh", "brightness", "up", NULL};
static const char *brightness_down[] = {"dwm-utils.sh", "brightness", "down", NULL};
static const char *cmdsoundup[] = {"dwm-utils.sh", "vol", "up", NULL};
static const char *cmdsounddown[] = {"dwm-utils.sh", "vol", "down", NULL};
static const char *cmdsoundtoggle[] = {"dwm-utils.sh", "vol", "toggle", NULL};
static const char *cmdsoundnext[] = {"playerctl", "next", NULL};
static const char *cmdsoundprev[] = {"playerctl", "previous", NULL};
static const char *cmdsoundplaypause[] = {"playerctl", "play-pause", NULL};
static const char *cmdpaste[] = {"sh", "-x", "xclip -o | paste", NULL};
static const char *cmdcalc[] = {"st", "-e", "bc", NULL};
static const char *cmdhtop[] = {"st", "-e", "htop", NULL};
static const char *cmdwicd[] = {"st", "-e", "wicd-curses", NULL};
static const char *cmdsleep[] = {"sh","-c","sudo systemctl suspend-then-hibernate", NULL};
static const char *cmdsearch[] = {"dwm-utils.sh", "search", NULL};
static const char *cmdlock[] = {"sh","-c","xautolock -locknow"};
static const char *cmddpmsoff[] = {"sh","-c","sleep 1 && xset dpms force off", NULL};
static const char *cmdopen[] = {"/home/krnk/bin/dmenufilebrowser", NULL};
static const char *cmdkbd[] = {"svkbd", NULL};
static const char *cmdprintscreen[] = {"flameshot","gui", NULL};
static const char *cmdfm[] = {"st", "-e", "rfm", NULL};
static const char *cmdbg[] = {"dwm-utils.sh", "background", NULL};
static const char *cmdbg_solid[] = {"sh","-c","feh  --no-fehbg --bg-tile --randomize  $HOME/Pictures/Wallpapers/Tiles",  NULL};
static const char *cmdwshiftinp[] = {"dwm-utils.sh", "add_bigscreen", NULL};
static const char *cmdwinp[] = {"/home/krnk/bin/screenscaling", NULL};
static const char *cmdtogglecompton[] = {"dwm-utils.sh", "compton", NULL};
static const char *cmdemrg[] = {"sudo","sh", "-c","echo 1 > /proc/sys/kernel/sysrq && sync  && echo o > /proc/sysrq-trigger", NULL};
/* tagging */
//static const char *tags[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
static const char *tags[] = {
	"One",
	"Two",
	"Three",
	"Four",
	"Five",
	"Six",
	"Seven",
	"Eight",
	"Nine"};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      		instance    title       tags mask     isfloating   isterminal noswallow monitor */
	//{"Gimp", NULL, NULL, 0, 1, 0, 0, -1},
	//{"mate-terminal", NULL, NULL, 0, 0, 1, 0, -1},
	{"svkbd", NULL, NULL, 0, 1, 0, 1, -1},
	{"st", NULL, NULL, 0, 0, 1, 0, -1},
	{"xterm", NULL, NULL, 0, 0, 1, 0, -1},
	{"alacritty", NULL, NULL, 0, 0, 1, 0, -1},
	{ panel[1],   NULL,NULL,(1 << 9) - 1, 0,           -1 },
};

/* layout(s) */
static const float mfact = 0.75;  /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;	  /* number of clients in master area */
static const int resizehints = 0; /* 1 means respect size hints in tiled resizals */
/* Bartabgroups properties */
#define BARTAB_BORDERS 0	   // 0 = off, 1 = on
#define BARTAB_BOTTOMBORDER 1  // 0 = off, 1 = on
#define BARTAB_TAGSINDICATOR 0 // 0 = off, 1 = on if >1 client/view tag, 2 = always on
#define BARTAB_TAGSPX 10	   // # pixels for tag grid boxes
#define BARTAB_TAGSROWS 1	   // # rows in tag grid (9 tags, e.g. 3x3)

#include "layouts.c"

static void (*bartabmonfns[])(Monitor *) = {monocle /* , customlayoutfn */};
static void (*bartabfloatfns[])(Monitor *) = {NULL /* , customlayoutfn */};

static const Layout layouts[] = {
	/* symbol     arrange function */

	{"a", auto_layout}, /* first entry is default */
	{"f", fibonacci},
	{"m", monocle},
	{"s", speed_dating},
	{"u", centeredmaster},
	{"b", bstack},
	{"d", fixed_dual},
	{"t", tatami},
	// {"q", fixed_quad},
	//	{"===", bstackhoriz},
	{NULL, NULL},
};

/* key definitions */
#define MODKEY Mod1Mask
#define SUPER Mod4Mask
#define XF86AudioMute 0x1008ff12
#define XF86AudioLowerVolume 0x1008ff11
#define XF86AudioRaiseVolume 0x1008ff13
#define XF86MonBrightnessDown 0x1008ff03
#define XF86MonBrightnessUp 0x1008ff02
#define XF86AudioPrev 0x1008ff16
#define XF86AudioPlay 0x1008ff14
#define XF86AudioNext 0x1008ff17
#define XF86Calc 0x1008ff1d
#define XF86Search 0x1008ff1b
#define TAGKEYS(KEY, TAG)                                          \
	{MODKEY, KEY, view, {.ui = 1 << TAG}},                         \
		{MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}}, \
		{MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},          \
		{MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                           \
	{                                                        \
		.v = (const char *[]) { "/bin/sh", "-c", cmd, NULL } \
	}

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {"dmenu_run", "-m", dmenumon, NULL};
// static const char *dmenucmd[] = {"rofi","-show","run", NULL};
// static const char *dockermenucmd[] = {"dockermenu_run", "-m", dmenumon, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL};
static const char *termcmd[] = {"st", "-e", "bash", NULL};
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        					function        argument */
	{MODKEY, XK_p, spawn, {.v = dmenucmd}},
	//	{MODKEY | SUPER, XK_p, spawn, {.v = dockermenucmd}},
	{MODKEY | ShiftMask, XK_Return, spawn, {.v = termcmd}},
	{MODKEY, XK_b, togglebar, {0}},
	{MODKEY, XK_j, focusstack, {.i = +1}},
	{MODKEY, XK_k, focusstack, {.i = -1}},
	{MODKEY, XK_i, incnmaster, {.i = +1}},
	{MODKEY, XK_d, incnmaster, {.i = -1}},
	{MODKEY, XK_h, setmfact, {.f = -0.025}},
	{MODKEY, XK_l, setmfact, {.f = +0.025}},
	{MODKEY, XK_Return, zoom, {0}},
	{MODKEY, XK_Tab, view, {0}},
	{MODKEY | ShiftMask, XK_c, killclient, {0}},
	{SUPER | ShiftMask, XK_p, spawn, {.v = cmdwshiftinp}},
	{SUPER , XK_s, spawn, {.v = cmdsleep}},
	{SUPER , XK_v, spawn, {.v = cmdpaste}},
	{SUPER , XK_Home, spawn, {.v = cmdwicd}},
	{0 , XK_Print, spawn, {.v = cmdprintscreen}},
	// {MODKEY, XK_t, setlayout, {.v = &layouts[1]}},
	// {MODKEY, XK_f, setlayout, {.v = &layouts[0]}},
	// {MODKEY, XK_m, setlayout, {.v = &layouts[3]}},
	// {MODKEY, XK_u, setlayout, {.v = &layouts[4]}},
	// {MODKEY, XK_o, setlayout, {.v = &layouts[5]}},
	//{ MODKEY, XK_s, setlayout, {.v = &layouts[3]} },
	{MODKEY | ControlMask, XK_comma, cyclelayout, {.i = -1}},
	{MODKEY | ControlMask, XK_period, cyclelayout, {.i = +1}},
	{MODKEY | ShiftMask, XK_j, movestack, {.i = +1}},
	{MODKEY | ShiftMask, XK_k, movestack, {.i = -1}},
	{MODKEY, XK_space, setlayout, {0}},

	{MODKEY | ShiftMask, XK_space, togglefloating, {0}},
	{MODKEY ,XK_s,togglesticky, {0} },
	{MODKEY, XK_0, view, {.ui = ~0}},
	{MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
	//{MODKEY, XK_comma, focusmon, {.i = -1}},
	{MODKEY, XK_period, focusmon, {.i = +1}},
	{MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
	{MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},
	TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3) TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7) TAGKEYS(XK_9, 8)
	// {MODKEY, XK_minus, setgaps, {.i = -5}},
	// {MODKEY, XK_equal, setgaps, {.i = +5}},
	// {MODKEY | ShiftMask, XK_minus, setgaps, {.i = GAP_RESET}},
	// {MODKEY | ShiftMask, XK_equal, setgaps, {.i = GAP_TOGGLE}},
	{MODKEY | SUPER, XK_c, spawn, {.v = cmdtogglecompton}},
	{MODKEY | ShiftMask, XK_q, quit, {0}},
	// MAC style shortcuts
	{SUPER, XK_q, killclient, {0}},
	{SUPER |ShiftMask, XK_q, spawn, { .v = cmdemrg}},
	{SUPER, XK_o, spawn, {.v = cmdopen}},
	{MODKEY | ShiftMask, XK_r, quit_reload, {0}},
//	{SUPER, XK_p, toggle_display_scale, {0}},
	{SUPER , XK_p, spawn, {.v = cmdwinp}},
	{SUPER, XK_b, spawn, {.v = cmdbg}},
	{SUPER, XK_e, spawn, {.v = cmdfm}},
	{SUPER | ShiftMask, XK_b, spawn, {.v = cmdbg_solid}},
	{SUPER, XK_F7, spawn, {.v = cmddpmsoff}},
	{SUPER, XK_l, spawn, {.v = cmdlock}},
	{SUPER, XK_k, spawn, {.v = cmdkbd}},
	{0, XF86AudioMute, spawn, {.v = cmdsoundtoggle}},
	{0, XF86AudioRaiseVolume, spawn, {.v = cmdsoundup}},
	{0, XF86AudioLowerVolume, spawn, {.v = cmdsounddown}},
	{0, XF86MonBrightnessDown, spawn, {.v = brightness_down}},
	{0, XF86MonBrightnessUp, spawn, {.v = brightness_up}},
	{0, XF86AudioNext, spawn, {.v = cmdsoundnext}},
	{0, XF86AudioPlay, spawn, {.v = cmdsoundplaypause}},
	{0, XF86AudioPrev, spawn, {.v = cmdsoundprev}},
	{0, XF86Calc, spawn, {.v = cmdcalc}},
	{0, XF86Search, spawn, {.v = cmdsearch}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ClkLtSymbol, 0, Button1, cyclelayout, {.i = +1}},
	{ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
	{ClkWinTitle, 0, Button2, zoom, {0}},
	{ClkStatusText, 0, Button1, spawn, {.v = cmdhtop}},
	{ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
	{ClkStatusText, 0, Button3, spawn, {.v = cmdwicd}},
	{ClkClientWin, MODKEY, Button1, movemouse, {0}},
	{ClkClientWin, MODKEY, Button2, togglefloating, {0}},
	{ClkClientWin, MODKEY, Button3, resizemouse, {0}},
	{ClkTagBar, 0, Button1, view, {0}},
	{ClkTagBar, 0, Button3, toggleview, {0}},
	{ClkTagBar, MODKEY, Button1, tag, {0}},
	{ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
